# example-fastAPI

Ejemplo de una API sencilla creada con FastAPI.

## Instalar

Crear entorno virtual e instalar los paquetes necesarios:

* `pip install -r requirements.txt`

## Puesta en marcha

*  `uvicorn main:app` -> Levanta nuestra aplicación en el puerto por defecto 8000.

* http://127.0.0.1:8000